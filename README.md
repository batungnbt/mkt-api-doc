# API DOCUMENT
- Host: https://dev.mkt.city
- Base path: /api/v1
##### GET CAPTCHA
- Endpoint: /captcha-img
- Method: GET
- Response body:   
**key**  
**img** : base64 image
##### GET ACCESS TOKEN
- Endpoint: /login
- Method: POST
- Params:  
**username** or **email**  
**password**  
**key** : Key captcha  
**captcha**
- Response body:  
access_token : Token for authentication (added to Authorization header)
user_info : Logged user data
##### GET PROFILE
- Endpoint: /profiles/:profile_id
- Method: GET
- Params:  
profile_id
- Response body:
**profile_id**  
**profile_user** : User ID of profile creator  
**profile_hash**  
**profile_uid** : User ID of profile owner  
**profile_name** :  
**browser_version_id**  
**os_version_id**  
**proxy_id**  
**profile_useragent** : User-Agent of profile  
**profile_proxy** : ip:port:username:password  
**proxy_type** : Proxy connect  method  
**profile_note**  
**profile_group** : Group ID of profile  
**date_expired** : Date expired of profile  
**profile_preference** : Settings of profile  
**is_proxy_conf_auto** : Default advanced configuration from ip/proxy  
**upload** : Status of the profile cookie upload to oss  
**opened_last_at** : Profile time was last opened  
**is_open** : Profile is opening
##### CREATE PROFILE
- Endpoint: /profiles
- Method: POST
- Params:  
**profile_name**  
**profile_group**  
**profile_useragent**  
**proxy_type**  
**profile_proxy** : format ip:port:username:password  
**is_proxy_conf_auto**  
**timezone** : Timezone of profile.  
**location** : Location of profile.  
**local_ip** : Local IP of profile.  
- Response body: User data created
##### GET BACKUP FILE  OF PROFILE
- Endpoint: /profiles/:profile_id/backup
- Method: GET
- Params:  
**profile_id**  
- Response body:  
**signurl** : Link download file backup
##### BACKUP PROFILE
- Endpoint: /profiles/:profile_id/backup
- Method: POST
- Params:  
**backup_file** : File backup  
- Response body:  
**status** : Status backup  
**message**